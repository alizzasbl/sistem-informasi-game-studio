package com.darmawan.sisteminformasi;

import android.content.Intent;
import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.net.Uri;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener
    {
        LinearLayout studio1, studio2, studio3, studio4, studio5, studio6;
        ImageView contact1, web1, map1, guestbook1;
        ImageView contact2, web2, map2, guestbook2;
        ImageView contact3, web3, map3, guestbook3;
        ImageView contact4, web4, map4, guestbook4;
        ImageView contact5, web5, map5, guestbook5;
        ImageView contact6, web6, map6, guestbook6;

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_main);

            studio1 = findViewById(R.id.studio1);
            studio2 = findViewById(R.id.studio2);
            studio3 = findViewById(R.id.studio3);
            studio4 = findViewById(R.id.studio4);
            studio5 = findViewById(R.id.studio5);
            studio6 = findViewById(R.id.studio6);
            studio1.setOnClickListener(this);
            studio2.setOnClickListener(this);
            studio3.setOnClickListener(this);
            studio4.setOnClickListener(this);
            studio5.setOnClickListener(this);
            studio6.setOnClickListener(this);

            contact1 = findViewById(R.id.contact1);
            contact2 = findViewById(R.id.contact2);
            contact3 = findViewById(R.id.contact3);
            contact4 = findViewById(R.id.contact4);
            contact5 = findViewById(R.id.contact5);
            contact6 = findViewById(R.id.contact6);
            contact1.setOnClickListener(this);
            contact2.setOnClickListener(this);
            contact3.setOnClickListener(this);
            contact4.setOnClickListener(this);
            contact5.setOnClickListener(this);
            contact6.setOnClickListener(this);

            web1 = findViewById(R.id.web1);
            web2 = findViewById(R.id.web2);
            web3 = findViewById(R.id.web3);
            web4 = findViewById(R.id.web4);
            web5 = findViewById(R.id.web5);
            web6 = findViewById(R.id.web6);
            web1.setOnClickListener(this);
            web2.setOnClickListener(this);
            web3.setOnClickListener(this);
            web4.setOnClickListener(this);
            web5.setOnClickListener(this);
            web6.setOnClickListener(this);

            map1 = findViewById(R.id.map1);
            map2 = findViewById(R.id.map2);
            map3 = findViewById(R.id.map3);
            map4 = findViewById(R.id.map4);
            map5 = findViewById(R.id.map5);
            map6 = findViewById(R.id.map6);
            map1.setOnClickListener(this);
            map2.setOnClickListener(this);
            map3.setOnClickListener(this);
            map4.setOnClickListener(this);
            map5.setOnClickListener(this);
            map6.setOnClickListener(this);

            guestbook1 = findViewById(R.id.guestbook1);
            guestbook2 = findViewById(R.id.guestbook2);
            guestbook3 = findViewById(R.id.guestbook3);
            guestbook4 = findViewById(R.id.guestbook4);
            guestbook5 = findViewById(R.id.guestbook5);
            guestbook6 = findViewById(R.id.guestbook6);
            guestbook1.setOnClickListener(this);
            guestbook2.setOnClickListener(this);
            guestbook3.setOnClickListener(this);
            guestbook4.setOnClickListener(this);
            guestbook5.setOnClickListener(this);
            guestbook6.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            Intent intent = null;
            switch (v.getId()) {
                case R.id.studio1:
                case R.id.studio2:
                case R.id.studio3:
                case R.id.studio4:
                case R.id.studio5:
                case R.id.studio6:
                    intent = new Intent(this, DetailActivity.class);
                    startActivity(intent);
                    break;

                case R.id.web1:
                    intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://agate.id/"));

                    startActivity(intent);
                    break;
                case R.id.web2:
                    intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.touchten.com/"));

                    startActivity(intent);
                    break;
                case R.id.web3:
                    intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://arsanesia.com/"));

                    startActivity(intent);
                    break;
                case R.id.web4:
                    intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://mojikenstudio.com/"));

                    startActivity(intent);
                    break;
                case R.id.web5:
                    intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.digitalhappiness.net/"));

                    startActivity(intent);
                    break;
                case R.id.web6:
                    intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.gameloft-sea.com/"));

                    startActivity(intent);
                    break;

                case R.id.contact1:
                    intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:022 2010665"));

                    startActivity(intent);
                    break;
                case R.id.contact2:
                    intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:021 31997594"));

                    startActivity(intent);
                    break;
                case R.id.contact3:
                    intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:022 6015834"));

                    startActivity(intent);
                    break;
                case R.id.contact4:
                    intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:0878-7781-1899"));

                    startActivity(intent);
                    break;
                case R.id.contact5:
                    intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:022 20452158"));

                    startActivity(intent);
                    break;
                case R.id.contact6:
                    intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:0274 4469477"));

                    startActivity(intent);
                    break;

                case R.id.map1:
                case R.id.map2:
                case R.id.map3:
                case R.id.map4:
                case R.id.map5:
                case R.id.map6:
                    Intent intent2 = new Intent(this, MapActivity.class);
                    startActivity(intent2);
                    break;

                case R.id.guestbook1:
                case R.id.guestbook2:
                case R.id.guestbook3:
                case R.id.guestbook4:
                case R.id.guestbook5:
                case R.id.guestbook6:
                    Intent intent3 = new Intent(this, GuestBookActivity.class);
                    startActivity(intent3);
                    break;
            }
        }

        @Override
        protected void onActivityResult(int requestCode, int resultCode, Intent data) {
            super.onActivityResult(requestCode, resultCode, data);
            if(resultCode == Activity.RESULT_OK && requestCode == 0){
                String result = data.toURI();
                Toast.makeText(this, result, Toast.LENGTH_LONG);
            }
        }
    }
